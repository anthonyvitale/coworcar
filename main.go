package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	// "github.com/oliamb/cutter"
)

type page struct {
	Title    string
	ImageUrl string
}

func viewHandler(w http.ResponseWriter, req *http.Request) {
	p := &page{Title: "Cow Or Car", ImageUrl: "/imgs/cow.png"}
	renderTemplate(w, "index", p)
}

func renderTemplate(w http.ResponseWriter, tmpl string, p *page) {
	t, _ := template.ParseFiles(fmt.Sprintf("html/%s.html", tmpl))
	t.Execute(w, p)
}

func main() {
	fs := http.FileServer(http.Dir("imgs"))
	http.Handle("/imgs/", http.StripPrefix("/imgs/", fs))

	http.HandleFunc("/", viewHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
